// import {AuthenticationError} from 'apollo-server';
import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'community' || 'team';

module.exports = {

  Mutation: {
    changeCircle: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args.input.members_ids) {
        args.input.members_ids = args.input.members_ids.map((e) => new ObjectId(e));
      }

      if (args._id) {
        return await query(collectionItemActor, { type: 'circle', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'circle', input: args.input }, global.actor_timeout);
    },

    addMemberCircle: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, {
        type: 'circle',
        search: { _id: args._id },
        input:
                    {
                      $addToSet: { members_ids: new ObjectId(args.member_id) },
                    },

      }, global.actor_timeout);
    },

    removeMemberCircle: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, {
        type: 'circle',
        search: { _id: args._id },
        input:
                    { $pull: { members_ids: new ObjectId(args.member_id) } },
      }, global.actor_timeout);
    },
  },
  Query: {

    getCircle: async (obj, args, ctx, info) => {
      let circles = await ctx.db.circle.aggregate(
        [
          { $match: { _id: new ObjectId(args.id) } },
          {
            $lookup: {
              from: 'user',
              localField: 'members_ids',
              foreignField: '_id',
              as: 'members',
            },
          },
          { $limit: 1 },
        ],
      );

      let circle = circles[0];
      return circle || {};

      // return await ctx.db.circle.findOne({_id: new ObjectId(args.id)});
    },

    getCircleByTelegramId: async (obj, args, ctx, info) => {
      let circles = await ctx.db.circle.aggregate(
        [
          { $match: { telegram_id: args.telegram_id } },
          {
            $lookup: {
              from: 'user',
              localField: 'members_ids',
              foreignField: '_id',
              as: 'members',
            },
          },
          { $limit: 1 },
        ],
      );

      let circle = circles[0];
      return circle || {};

      // return await ctx.db.circle.findOne({_id: new ObjectId(args.id)});
    },

    getCircles: async (obj, args, ctx, info) => await ctx.db.circle.aggregate(
      [
        { $sort: { name: 1 } },
        {
          $lookup: {
            from: 'user',
            localField: 'members_ids',
            foreignField: '_id',
            as: 'members',
          },
        },
      ],
    ),

    // return await ctx.db.circle.find();

  },
  Circle: {
    members: async (obj, args, context, info) => {
      if (obj.members) {
        obj.members.forEach((user) => { user.full_name = `${user.name || ''} ${user.family_name || ''}`; });
      }
      return obj.members;
    },
  },

};
